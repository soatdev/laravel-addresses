<?php
namespace Soatdev\Addresses\Controllers;
 
use \App\Http\Controllers\Controller;
use Soatdev\Addresses\Addresses;
 
class AddressesController extends Controller {
    public function foo() {
    	// Init
	    $page_title      = '2 Bananas';
	    $welcome_message = 'Welcome! Welcome! Have 2 bananas.';
	 
	    $data = compact('page_title', 'welcome_message');
    	return view('addresses::welcome', $data);
    	//return Addresses::saySomething();
        //return 'The controller works!';
    }
}