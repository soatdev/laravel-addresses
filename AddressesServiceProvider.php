<?php

namespace Soatdev\Addresses;

use Illuminate\Support\ServiceProvider;

class AddressesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Route
        include __DIR__.'/routes.php';

        // Language
        $this->loadTranslationsFrom( __DIR__.'/Lang', 'addresses');

        $this->publishes([
            __DIR__.'/Config/addresses.php' => config_path('addresses.php'),
            ], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Config
        $this->mergeConfigFrom( __DIR__.'/Config/addresses.php', 'addresses');

        // View
        $this->loadViewsFrom(__DIR__ . '/Views', 'addresses');

        $this->app['addresses'] = $this->app->share(function($app) {
            return new Addresses;
        });
    }
}
