<?php 
namespace Soatdev\Addresses\Facade;
 
use Illuminate\Support\Facades\Facade;
 
class Addresses extends Facade {
 
    protected static function getFacadeAccessor() { return 'addresses'; }
 
}